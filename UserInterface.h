/*
 * UserInterface.h
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef USERINTERFACE_H_
#define USERINTERFACE_H_

#include "VendingMachine.h"

class UserInterface {
public:
	UserInterface();
	~UserInterface();

	const VendingMachine* getMachine() const {return machine;}
	void setMachine(VendingMachine* machine) {this->machine = machine;}
	void runSystem ();
	void runBuyingProcedure();

private:
	VendingMachine* machine;
	const Product* chosenProduct[maxArraySize] = { };

	void printProducts ();
	void chooseProduct ();
	void enterMoney();
	void acceptOrder();
	void giveChange();
	void deliverProduct();
};

#endif /* USERINTERFACE_H_ */
