/*
 * VendingMachine.h
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef VENDINGMACHINE_H_
#define VENDINGMACHINE_H_

#include <string>

const int maxArraySize (100);

struct Product{
	Product();
	~Product();

	std::string productName;
	float productPrice;
	int productAmount;
	int chosenAmount;
};

class VendingMachine {
public:
	VendingMachine();
	~VendingMachine();
	Product* chooseProduct (std::string productName);
	bool validateEnteredCoin(float coin);
	bool isEnteredMoneyEnough ();
	void giveChange ();
	void deliverProduct ();
	void addProductToMachine (std::string productName, float productPrice, int productAmount);

	const Product& operator[] (const int index) const {return product[index];}

	const Product* getProduct() const {return product;}
	const Product* getProduct(const int index) const {return &product[index];}
	float getEnteredMoney() const {return enteredMoney;}
	float getPriceOfProducts() const {return priceOfProducts;}
	void setEnteredMoney (float enteredMoney) {this->enteredMoney = enteredMoney;}
	void setIsOrderAccepted(bool isOrderAccepted) {this->isOrderAccepted = isOrderAccepted;}
	bool getIsOrderAccepted() const {return isOrderAccepted;}

private:
	Product product[maxArraySize];
	float priceOfProducts;
	float enteredMoney;
	float change[maxArraySize] = { };
	float* ptrChange;
	bool isOrderAccepted;

	void setChange();
	void setChangeNominal (float nominal, float* remainingMoney);

};

#endif /* VENDINGMACHINE_H_ */
