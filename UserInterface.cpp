/*
 * UserInterface.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "UserInterface.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;

UserInterface::UserInterface() {
}

UserInterface::~UserInterface() {
}
void UserInterface::printProducts() {
	cout << "Product\t\tPrice\t\tAvailability\n";
	const Product* product = machine->getProduct();
	for (int i = 0; i < maxArraySize; ++i) {
		if (product->productName == "")
			break;
		cout << std::fixed << std::setprecision(2);
		cout << product->productName << "\t\t" << product->productPrice
				<< " euro\t" << product->productAmount-product->chosenAmount << "\n";
		++product;
	}
	cout << "Type Q and press enter to quit.\n";
}
void UserInterface::chooseProduct() {
	int i(0);
	int option(1);
	while (option != 2) {
		std::string productName("empty");
		chosenProduct[i] = machine->chooseProduct(productName);
		while (chosenProduct[i]->productName == "") {
			printProducts();
			cout << "Enter product name to choose one: ";
			cin >> productName;
			if (productName == "Q")
				break;
			chosenProduct[i] = machine->chooseProduct(productName);
			if ((chosenProduct[i]->productName == ""))
				cout << "Product in unavailable.\n";
		}
		++i;
		cout << "Would you like to choose anything else?\n";
		cout << "1) Yes\n";
		cout << "2) No\n";
		cin >> option;
		if (option != 1 && option != 2)
			cout << "Invalid option chosen. Try again. \n";
	}
}
void UserInterface::enterMoney() {
	float enteredMoney(0);
	cout << "Please enter " << machine->getPriceOfProducts() << " euro\n";
	while (machine->isEnteredMoneyEnough() == 0) {
		cout << "Entered: " << enteredMoney << "\tNeeded: "
				<< (machine->getPriceOfProducts() - enteredMoney) << "\n";
		cout << "Type 0 and press enter to cancel and collect your money\nor insert coin to continue:\n";
		float coin;
		cin >> coin;
		if (coin) {
			if (machine->validateEnteredCoin(coin)) {
				enteredMoney += coin;
				machine->setEnteredMoney(enteredMoney);
			} else
				cout
						<< "Invalid coin entered. Machine accepts only:\n0.10, 0.20, 0.50, 1, 2 and 5 euro coins.\n";
		} else
			return;
	}
	machine->setIsOrderAccepted(1);
}
void UserInterface::giveChange() {
	cout << "Collect your change.\n";
	machine->giveChange();
}
void UserInterface::deliverProduct() {
	cout << "Here goes yours ";
	int i(0);
	while (chosenProduct[i] && i < maxArraySize) {
		cout << chosenProduct[i]->productName;
		++i;
		if (chosenProduct[i] && i < maxArraySize)
			cout << ", ";
	}
	machine->deliverProduct();
	cout << "!\nThank you! See you soon!\n";
}
void UserInterface::runBuyingProcedure(){
	chooseProduct();
	enterMoney();
	giveChange();
	if (machine->getIsOrderAccepted())
	deliverProduct();
}
