/*
 * testingVendingMachine.cpp
 *
 *  Created on: 04.05.2017
 *      Author: marce
 */

#include "gtest/gtest.h"
#include "VendingMachine.h"

class VendingMachineOperationTestFixture : public testing::Test {
protected:
	virtual void SetUp() {	}
	virtual void TearDown() {}
	VendingMachine vMachine;
};

TEST_F (VendingMachineOperationTestFixture, VendingMaChineContructionCalledCorrectly){
	EXPECT_FALSE(vMachine.getIsOrderAccepted());
	EXPECT_EQ(0, vMachine.getEnteredMoney());
	EXPECT_EQ(0, vMachine.getPriceOfProducts());
	EXPECT_EQ("Mars", vMachine.getProduct(0)->productName);
	EXPECT_FLOAT_EQ(2, vMachine.getProduct(0)->productPrice);
	EXPECT_EQ(2, vMachine.getProduct(0)->productAmount);
	EXPECT_EQ("Bueno", vMachine.getProduct(4)->productName);
	EXPECT_FLOAT_EQ(3.2, vMachine.getProduct(4)->productPrice);
	EXPECT_EQ(2, vMachine.getProduct(4)->productAmount);
}
