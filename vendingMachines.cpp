//============================================================================
// Name        : vendingMachines.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "gtest/gtest.h"
#include "VendingMachine.h"
#include "UserInterface.h"

using namespace std;

int main(int argc, char **argv) {

//	::testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS();

	UserInterface uInterface;
	VendingMachine vMachine;

	vMachine.addProductToMachine("Mars", 2, 2);
	vMachine.addProductToMachine("Kitkat", 2.6, 3);

	uInterface.setMachine(&vMachine);

	uInterface.runBuyingProcedure();

	return 0;
}
