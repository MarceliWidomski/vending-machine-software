/*
 * VendingMachine.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "VendingMachine.h"
#include <iostream>

Product::Product() {
	this->productName = "";
	this->productPrice = 0;
	this->productAmount = 0;
	this->chosenAmount = 0;
}
Product::~Product() {
}
VendingMachine::VendingMachine() {
	ptrChange = change;
	isOrderAccepted = 0;
	priceOfProducts = 0;
	enteredMoney = 0;
	product[0].productName = "Mars";
	product[0].productPrice = 2.00;
	product[0].productAmount = 2;
	product[1].productName = "Twix";
	product[1].productPrice = 2.20;
	product[1].productAmount = 2;
	product[2].productName = "Bounty";
	product[2].productPrice = 2.50;
	product[2].productAmount = 2;
	product[3].productName = "3Bit";
	product[3].productPrice = 2.40;
	product[3].productAmount = 2;
	product[4].productName = "Bueno";
	product[4].productPrice = 3.20;
	product[4].productAmount = 2;
}
VendingMachine::~VendingMachine() {
}
void VendingMachine::addProductToMachine(std::string productName,
		float productPrice, int productAmount) {
	for (int i = 0; i < maxArraySize; ++i) {
		if (product[i].productName == productName) {
			product[i].productAmount += productAmount;
			break;
		} else if (!product[i].productPrice) {
			product[i].productName = productName;
			product[i].productPrice = productPrice;
			product[i].productAmount = productAmount;
			break;
		}
	}
}
Product* VendingMachine::chooseProduct(std::string productName) {
	int i(0);
	for (i = 0; i < maxArraySize; ++i) {
		bool isAvailable = product[i].productAmount - product[i].chosenAmount;
		if (product[i].productName == productName && isAvailable) {
			priceOfProducts += product[i].productPrice;
			++product[i].chosenAmount;
			return &product[i];
		} else if (product[i + 1].productName == "") {
			return &product[i + 1];
			break;
		}
	}
}
bool VendingMachine::validateEnteredCoin(float coin) {
	if (coin == 5 || coin == 2 || coin == 1 || coin == 0.5f || coin == 0.2f || coin == 0.1f)
		return 1;
	else
		return 0;
}
bool VendingMachine::isEnteredMoneyEnough() {
	if (enteredMoney >= priceOfProducts)
		return 1;
	else
		return 0;
}
void VendingMachine::setChangeNominal(float nominal, float* remainingMoney) {
	while (*remainingMoney >= nominal) {
		*remainingMoney -= nominal;
		*ptrChange = nominal;
		++ptrChange;
	}
}
void VendingMachine::setChange() {
	float remainingMoney;
	if(isOrderAccepted)
	remainingMoney = enteredMoney - priceOfProducts;
	else
	remainingMoney = enteredMoney;
	ptrChange = change;
	setChangeNominal(5, &remainingMoney);
	setChangeNominal(2, &remainingMoney);
	setChangeNominal(1, &remainingMoney);
	setChangeNominal(0.5, &remainingMoney);
	setChangeNominal(0.2, &remainingMoney);
	setChangeNominal(0.1, &remainingMoney);
}
void VendingMachine::giveChange() {
	setChange();
	ptrChange = change;
	while (*ptrChange) {
		std::cout << *ptrChange << " euro\n";
		++ptrChange;
	}
	enteredMoney = 0;
	ptrChange = change;
	change[maxArraySize] = { };

}
void VendingMachine::deliverProduct() {
	int i (0);
	while (product[i].productName != ""){
		product[i].productAmount -=product[i].chosenAmount;
		++i;
	}
	this->priceOfProducts = 0;
}
